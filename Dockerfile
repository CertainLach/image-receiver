FROM rust:1.48.0 AS builder
WORKDIR /usr/src/
RUN rustup target add x86_64-unknown-linux-musl
RUN apt-get update && apt-get install -y musl-tools
# Cache dependencies to new layer
RUN USER=root cargo new image-uploader
WORKDIR /usr/src/image-uploader
COPY Cargo.toml Cargo.lock ./
RUN cargo build --release
# Build app itself
COPY src ./src
RUN cargo install --target x86_64-unknown-linux-musl --path .

FROM scratch
COPY --from=builder /usr/local/cargo/bin/image-uploader .
USER 1000
EXPOSE 8000
CMD ["./image-uploader"]
