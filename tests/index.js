const chai = require("chai");
const chaiHttp = require("chai-http");
const expect = chai.expect
const baseUrl = process.env.SERVER || 'http://localhost:8000';
const { readFileSync } = require('fs');
chai.use(chaiHttp);
describe("Basic functionality", () => {
    it('server is live', (done) => {
        chai.request(baseUrl)
            .get('/')
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                done();
            });
    });
    it('server handles multipart', (done) => {
        chai.request(baseUrl)
            .post('/multipart')
            .field('image', readFileSync('./fixtures/example.png'))
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                console.log(res.body)
                expect(res.body.Successful).to.be.ok;
                expect(res.body.Successful).to.be.lengthOf(1);
                expect(res.body.Successful[0].Successful).to.be.ok;
                expect(res.body.Successful[0].Successful.thumbnail_path).to.be.ok;
                done();
            });
    });
    it('server handles json', (done) => {
        chai.request(baseUrl)
            .post('/json')
            .type('json')
            .send([{
                Inline: readFileSync('./fixtures/example.png').toString('base64'),
            }])
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res).to.be.json;
                expect(res.body.Successful).to.be.ok;
                expect(res.body.Successful).to.be.lengthOf(1);
                expect(res.body.Successful[0].Successful).to.be.ok;
                expect(res.body.Successful[0].Successful.thumbnail_path).to.be.ok;
                done();
            });
    });
});

describe('Failures', () => {
    it('rejects very large images', (done) => {
        chai.request(baseUrl)
            .post('/json')
            .type('json')
            .send([{
                Inline: Buffer.alloc(1024 * 1024 * 20).toString('base64'),
            }])
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.body.Successful[0].Error.code).to.equal(1)
                done();
            });
    });
    it('rejects not images', (done) => {
        chai.request(baseUrl)
            .post('/json')
            .type('json')
            .send([{
                Inline: Buffer.alloc(10).toString('base64'),
            }])
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.body.Successful[0].Error.code).to.equal(5)
                done();
            });
    });
    it('rejects bad input supplied as base64', (done) => {
        chai.request(baseUrl)
            .post('/json')
            .type('json')
            .send([{
                Inline: Buffer.from([0b10000000]),
            }])
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(400);
                done();
            });
    });
    it('rejects malformed urls', (done) => {
        chai.request(baseUrl)
            .post('/json')
            .type('json')
            .send([{
                Url: 'badproto://',
            }])
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(200);
                expect(res.body.Successful[0].Error.code).to.equal(11)
                done();
            });
    });
    it('rejects too large batches', (done) => {
        chai.request(baseUrl)
            .post('/json')
            .type('json')
            .send(new Array(200).fill({ Url: 'nothing' }))
            .end((err, res) => {
                expect(err).to.be.null;
                expect(res).to.have.status(400);
                expect(res.body.Error.code).to.equal(2)
                done();
            });
    });
});
