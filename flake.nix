{
  description = "image uploading service";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs-mozilla = {
      url = "github:mozilla/nixpkgs-mozilla";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, nixpkgs-mozilla, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let pkgs = import nixpkgs { inherit system; overlays = [ (import nixpkgs-mozilla) ]; }; in
        rec {
          devShell = import ./shell.nix { inherit pkgs; };
          packages.dockerImage = pkgs.dockerTools.buildImage {
            name = "image-uploader";
            config = {
              Cmd = [ "${defaultPackage}/bin/image-uploader" ];
            };
          };
          defaultPackage = pkgs.rustPlatform.buildRustPackage {
            pname = "image-uploader";
            version = "0.1.0";
            src = self;
            nativeBuildInputs = with pkgs; [ rustc cargo ];

            cargoSha256 = "2ZtkLruhWdI05MfNhOTxg4JrdAtcOfOd2Tt52928UWg=";
          };
        }
      );
}
