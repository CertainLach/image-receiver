use std::string::FromUtf8Error;

use actix_multipart::MultipartError;
use rusoto_core::RusotoError;
use rusoto_s3::PutObjectError;
use thiserror::Error;
use tokio::task::JoinError;

use crate::graceful_shutdown::ServerShuttingDownError;

#[derive(Error, Debug)]
pub enum Error {
    #[error("server is stopping and not available for further requests")]
    ServerIsStopping(#[from] ServerShuttingDownError),

    #[error("image exceeded size limit")]
    ImageIsTooBig,
    #[error("too many images sent")]
    MaxBatchSizeExceeded,
    #[error("request is too slow")]
    RequestIsTooSlow,
    #[error("uploaded file is not valid image: {0}")]
    InvalidImageFormat(std::io::Error),
    #[error("image is corrupt: {0}")]
    ImageDecodingError(#[from] image::ImageError),
    #[error("malformed multipart request: {0}")]
    MultipartError(#[from] MultipartError),
    #[error("malformed string: {0}")]
    MalformedString(FromUtf8Error),
    #[error("malformed url: {0}")]
    MalformedUrl(#[from] url::ParseError),

    #[error("timed out waiting for headers")]
    TimedOutResponse,
    #[error("timed out waiting for body")]
    TimedOutBody,

    #[error("request error: {0}")]
    ReqwestError(reqwest::Error),

    #[error("io error: {0}")]
    IoError(#[from] std::io::Error),
    #[error("object put failed: {0}")]
    PutObjectError(#[from] RusotoError<PutObjectError>),
    #[error("join failed: {0}")]
    JoinError(#[from] JoinError),
}
impl Error {
    /// If true - this error shouldn't be displayed to user,
    /// and should be logged to sentry instead
    pub fn is_internal(&self) -> bool {
        matches!(self, Error::IoError(_)
            | Error::PutObjectError(_)
            | Error::JoinError(_))
    }
    pub fn code(&self) -> u32 {
        match self {
            Error::ServerIsStopping(_) => 0,
            Error::ImageIsTooBig => 1,
            Error::MaxBatchSizeExceeded => 2,
            Error::RequestIsTooSlow => 3,
            Error::InvalidImageFormat(_) => 4,
            Error::ImageDecodingError(_) => 5,
            Error::MultipartError(_) => 6,
            Error::MalformedString(_) => 7,
            Error::MalformedUrl(_) => 8,
            Error::TimedOutResponse => 9,
            Error::TimedOutBody => 10,
            Error::ReqwestError(_) => 11,
            Error::IoError(_) => 12,
            Error::PutObjectError(_) => 13,
            Error::JoinError(_) => 14,
        }
    }
}

pub type Result<T> = std::result::Result<T, Error>;
