use actix_web::{post, web::Json, Responder};
use serde::{Deserialize, Deserializer};

use crate::{
    common::upload_image,
    common::upload_image_by_url,
    config::*,
    error::{Error, Result},
    graceful_shutdown, response,
    response::ApiResponse,
    response::UploadedImage,
};

#[derive(Deserialize)]
enum JsonImage {
    Inline(#[serde(deserialize_with = "from_base64")] Vec<u8>),
    Url(String),
}

fn from_base64<'de, D>(deserializer: D) -> std::result::Result<Vec<u8>, D::Error>
where
    D: Deserializer<'de>,
{
    use serde::de::Error;
    String::deserialize(deserializer)
        .and_then(|string| base64::decode(&string).map_err(|err| Error::custom(err.to_string())))
}

async fn upload_image_json(json: Json<Vec<JsonImage>>) -> Result<Vec<ApiResponse<UploadedImage>>> {
    let _latch = graceful_shutdown::defer().await?;

    if json.0.len() > REQUEST_CONFIG.max_batch_size {
        return Err(Error::MaxBatchSizeExceeded);
    }

    let mut out: Vec<ApiResponse<UploadedImage>> = Vec::new();
    // OPTIMISE: Should array elements be uploaded concurrently?
    for file in json.0 {
        out.push(match file {
            JsonImage::Inline(body) => {
                if body.len() > REQUEST_CONFIG.max_image_size as usize {
                    Err(Error::ImageIsTooBig).into()
                } else {
                    upload_image(body).await.into()
                }
            }
            JsonImage::Url(url) => upload_image_by_url(url).await.into(),
        });
    }
    Ok(out)
}

#[post("/json")]
async fn endpoint(json: Json<Vec<JsonImage>>) -> impl Responder {
    response::respond(upload_image_json(json).await)
}
