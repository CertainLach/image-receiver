use actix_multipart::{Field, Multipart, MultipartError};
use actix_web::{post, web::Bytes, Responder};
use futures::StreamExt;
use tokio::time::timeout;

use crate::{
    common::upload_image,
    common::upload_image_by_url,
    config::*,
    error::{Error, Result},
    graceful_shutdown,
    response::ApiResponse,
    response::{self, UploadedImage},
};

/// Results implements FromIter, but not Extend, which is used by Stream::collect
/// https://github.com/rust-lang/futures-rs/issues/1833
struct ByteStreamCollector(std::result::Result<Vec<u8>, MultipartError>);
impl Default for ByteStreamCollector {
    fn default() -> Self {
        Self(Ok(Vec::new()))
    }
}
impl Extend<std::result::Result<Bytes, MultipartError>> for ByteStreamCollector {
    fn extend<T: IntoIterator<Item = std::result::Result<Bytes, MultipartError>>>(
        &mut self,
        iter: T,
    ) {
        if self.0.is_err() {
            return;
        }
        let inner = self.0.as_mut().unwrap();
        for bytes in iter {
            match bytes {
                Ok(b) => inner.extend_from_slice(&b),
                Err(e) => {
                    self.0 = Err(e);
                    return;
                }
            }
        }
    }
}

async fn read_field(field: &mut Field) -> Result<Vec<u8>> {
    let mut out = Vec::new();
    while let Some(bytes) = field.next().await {
        let bytes = bytes.map_err(Error::MultipartError)?;
        if out.len() + bytes.len() > REQUEST_CONFIG.max_image_size as usize {
            return Err(Error::ImageIsTooBig);
        }
        out.extend_from_slice(&bytes);
    }
    Ok(out)
}

async fn upload_field(mut field: Field) -> Result<UploadedImage> {
    if let Some(disp) = field.content_disposition() {
        if let Some(name) = disp.get_name() {
            if name == "url" {
                let body = String::from_utf8(read_field(&mut field).await?)
                    .map_err(Error::MalformedString)?;
                return upload_image_by_url(body).await;
            }
        }
    }
    upload_image(read_field(&mut field).await?).await
}

pub async fn upload_image_multipart(
    mut content: Multipart,
) -> Result<Vec<ApiResponse<UploadedImage>>> {
    let _latch = graceful_shutdown::defer().await?;

    let mut out: Vec<ApiResponse<UploadedImage>> = Vec::new();
    let mut count = 0;
    while let Some(item) = content.next().await {
        count += 1;
        if count > REQUEST_CONFIG.max_batch_size {
            return Err(Error::MaxBatchSizeExceeded);
        }
        let item = item.map_err(Error::MultipartError)?;
        let body = timeout(REQUEST_CONFIG.max_image_time, upload_field(item))
            .await
            .map_err(|_| Error::RequestIsTooSlow)?;
        out.push(body.into())
    }
    Ok(out)
}

#[post("/multipart")]
async fn endpoint(content: Multipart) -> impl Responder {
    response::respond(upload_image_multipart(content).await)
}
