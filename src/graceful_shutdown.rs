use lazy_static::lazy_static;
use std::sync::atomic::{AtomicBool, Ordering};
use tokio::sync::{RwLock, RwLockReadGuard};

static IS_STOPPING: AtomicBool = AtomicBool::new(false);
lazy_static! {
    static ref SHUTDOWN_LATCH: RwLock<()> = RwLock::new(());
}

pub fn init() {
    tokio::spawn(async {
        if tokio::signal::ctrl_c().await.is_err() {
            // Graceful shutdown is not available
            return;
        }
        IS_STOPPING.store(true, Ordering::SeqCst);
        SHUTDOWN_LATCH.write().await;
        std::process::exit(0);
    });
}

pub struct ShutdownPreventionLock(RwLockReadGuard<'static, ()>);

#[derive(Debug, thiserror::Error)]
#[error("server is shutting down")]
pub struct ServerShuttingDownError;

#[must_use]
pub async fn defer() -> Result<ShutdownPreventionLock, ServerShuttingDownError> {
    if IS_STOPPING.load(Ordering::SeqCst) {
        Err(ServerShuttingDownError)
    } else {
        Ok(ShutdownPreventionLock(SHUTDOWN_LATCH.read().await))
    }
}
