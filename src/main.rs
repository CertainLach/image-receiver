mod error;

mod image_processing;

mod config;
mod graceful_shutdown;

mod common;
mod endpoint;
mod response;

use config::REQUEST_CONFIG;

use actix_http::{http::header::ContentType, Response};
use actix_web::{get, rt::System, web::JsonConfig, web::PayloadConfig, App, HttpServer, Responder};
use tokio::task::LocalSet;

#[get("/")]
async fn index() -> impl Responder {
    Response::Ok()
        .set(ContentType::html())
        .body(include_str!("index.html"))
}

// Tokio runtime is required by rusoto and hyper, and actix-rt is required by actix-web
#[tokio::main]
async fn main() -> std::io::Result<()> {
    graceful_shutdown::init();

    let local = LocalSet::new();
    let sys = System::run_in_tokio("server", &local);
    HttpServer::new(|| {
        App::new()
            .app_data(PayloadConfig::new(REQUEST_CONFIG.max_request_size))
            .app_data(JsonConfig::default().limit(REQUEST_CONFIG.max_request_size))
            .service(index)
            .service(endpoint::multipart::endpoint)
            .service(endpoint::json::endpoint)
    })
    .bind("0.0.0.0:8000")?
    .run()
    .await?;
    sys.await?;
    Ok(())
}
