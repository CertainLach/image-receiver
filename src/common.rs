use std::io::Write;

use futures::StreamExt;
use lazy_static::lazy_static;
use rusoto_core::credential::{AwsCredentials, StaticProvider};
use rusoto_s3::{PutObjectRequest, S3Client, S3};
use sha2::Digest;
use tokio::time::timeout;

use crate::{
    config::*,
    error::{Error, Result},
    image_processing::{process_image, ProcessedImage},
    response::UploadedImage,
};

lazy_static! {
    static ref S3_CLIENT: S3Client = S3Client::new_with(
        rusoto_core::request::HttpClient::new().expect("failed to create HTTP client"),
        StaticProvider::from(AwsCredentials::default()),
        rusoto_core::Region::Custom {
            name: S3_CONFIG.name.clone(),
            endpoint: S3_CONFIG.endpoint.clone(),
        },
    );
}

// Consumes vec because this is most realible way to implement this,
// - s3 api requires object size to be known, but multipart fields doesn't have ability to know its own size
// - it can be done via temporary file, but images are mostly small, and tempfile would be overkill
// - preview creation is performed via OpenCV, and OpenCV doesn't support streaming
pub async fn upload_image(body: Vec<u8>) -> Result<UploadedImage> {
    let mut hash = sha2::Sha256::default();
    hash.write_all(&body)?;
    hash.flush()?;
    let hash = hash.finalize();

    let ProcessedImage {
        reencoded,
        thumbnail,
        dimensions,
    } = tokio::task::spawn_blocking(move || process_image(&body)).await??;
    let mut path: String = hex::encode(&hash);
    let mut thumbnail_path = path.clone();
    path.push_str(".png");
    thumbnail_path.push_str(".thumb.jpeg");

    let size = reencoded.len();
    let thumbnail_size = thumbnail.as_ref().map(|t| t.len());

    // OPTIMIZE: Put objects concurrently
    S3_CLIENT
        .put_object(PutObjectRequest {
            bucket: S3_CONFIG.bucket.clone(),
            key: path.clone(),
            body: Some(reencoded.into()),
            ..Default::default()
        })
        .await?;
    if let Some(thumbnail) = thumbnail {
        S3_CLIENT
            .put_object(PutObjectRequest {
                bucket: S3_CONFIG.bucket.clone(),
                key: thumbnail_path.clone(),
                body: Some(thumbnail.into()),
                ..Default::default()
            })
            .await?;
    }
    Ok(UploadedImage {
        path,
        size,
        thumbnail_path: thumbnail_size.map(|_| thumbnail_path),
        thumbnail_size,

        dimensions,
    })
}

pub async fn upload_image_by_url(url: String) -> Result<UploadedImage> {
    // OPTIMIZE: should downloading by url to be pushed to queue and then performed by external service?
    let url: reqwest::Url = url.parse()?;
    let response = timeout(URL_CONFIG.url_headers_timeout, reqwest::get(url))
        .await
        .map_err(|_| Error::TimedOutResponse)?
        .map_err(Error::ReqwestError)?;
    // fail fast if content-length is known
    if let Some(content_length) = response.content_length() {
        if content_length > REQUEST_CONFIG.max_image_size as u64 {
            dbg!("Content length {}", content_length);
            return Err(Error::ImageIsTooBig);
        }
    }
    let body = timeout(URL_CONFIG.url_body_timeout, async {
        let mut out = Vec::new();
        let mut stream = response.bytes_stream();
        while let Some(bytes) = stream.next().await {
            let bytes = bytes.map_err(Error::ReqwestError)?;
            if out.len() + bytes.len() > REQUEST_CONFIG.max_image_size as usize {
                dbg!(out.len(), bytes.len());
                return Err(Error::ImageIsTooBig);
            }
            out.extend_from_slice(&bytes);
        }
        Ok(out)
    })
    .await
    .map_err(|_| Error::TimedOutBody)??;
    upload_image(body).await
}
