use actix_http::{http::header::ContentType, Response};
use actix_web::Responder;
use serde::Serialize;

use crate::error::Result;

#[derive(serde::Serialize)]
pub struct UploadedImage {
    pub path: String,
    pub size: usize,
    pub thumbnail_path: Option<String>,
    pub thumbnail_size: Option<usize>,

    pub dimensions: (u32, u32),
}

#[derive(serde::Serialize)]
pub enum ApiResponse<T: Serialize> {
    Successful(T),
    Error { description: String, code: u32 },
}
impl<T: Serialize> From<Result<T>> for ApiResponse<T> {
    fn from(result: Result<T>) -> Self {
        match result {
            Ok(ok) => ApiResponse::Successful(ok),
            Err(e) => ApiResponse::Error {
                description: if e.is_internal() {
                    "internal error".to_owned()
                } else {
                    e.to_string()
                },
                code: e.code(),
            },
        }
    }
}

pub fn respond<T: Serialize>(result: Result<T>) -> impl Responder {
    match &result {
        Ok(_) => Response::Ok(),
        Err(e) if e.is_internal() => Response::InternalServerError(),
        Err(_) => Response::BadRequest(),
    }
    .set(ContentType::json())
    .body(serde_json::to_string(&From::from(result) as &ApiResponse<T>).unwrap())
}
