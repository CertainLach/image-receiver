use lazy_static::lazy_static;
use serde::Deserialize;
use std::time::Duration;

fn bucket() -> String {
    "images".into()
}

#[derive(Deserialize)]
pub struct S3Config {
    /// Region name
    pub name: String,
    /// Region endpoint
    pub endpoint: String,
    /// Bucket name
    #[serde(default = "bucket")]
    pub bucket: String,
}
lazy_static! {
    pub static ref S3_CONFIG: S3Config = envy::prefixed("S3_")
        .from_env()
        .expect("S3 config is not initialized");
}

fn max_image_size() -> usize {
    1024 * 1024 * 8
}

fn max_image_time() -> Duration {
    Duration::from_secs(5)
}

fn max_batch_size() -> usize {
    8
}

fn max_request_size() -> usize {
    max_image_size() * max_batch_size() + 1024
}

#[derive(Deserialize)]
pub struct RequestConfig {
    /// Maximum size of one image
    #[serde(default = "max_image_size")]
    pub max_image_size: usize,
    /// Max time allowed per image read
    /// To prevent usage of slowlorri/others
    #[serde(default = "max_image_time")]
    pub max_image_time: Duration,
    /// Maximum amount of images in single batch
    #[serde(default = "max_batch_size")]
    pub max_batch_size: usize,
    /// Maximum size of request (including images and headers)
    #[serde(default = "max_request_size")]
    pub max_request_size: usize,
}
lazy_static! {
    pub static ref REQUEST_CONFIG: RequestConfig = envy::prefixed("REQUEST_")
        .from_env()
        .expect("Request config is not initialized");
}

fn url_headers_timeout() -> Duration {
    Duration::from_secs(8)
}
fn url_body_timeout() -> Duration {
    max_image_time()
}

#[derive(serde::Deserialize)]
pub struct UrlConfig {
    /// Time allowed for requested resource to respond
    #[serde(default = "url_headers_timeout")]
    pub url_headers_timeout: Duration,
    /// Time allowed for requested resource to write image
    #[serde(default = "url_body_timeout")]
    pub url_body_timeout: Duration,
}
lazy_static! {
    pub static ref URL_CONFIG: UrlConfig = envy::prefixed("URL_")
        .from_env()
        .expect("Url config is not initialized");
}
