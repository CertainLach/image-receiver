use image::GenericImageView;
use image::{io::Reader, ImageOutputFormat};
use std::io::Cursor;

use crate::error::{Error, Result};

pub struct ProcessedImage {
    pub reencoded: Vec<u8>,
    pub thumbnail: Option<Vec<u8>>,

    pub dimensions: (u32, u32),
}

pub fn process_image(buf: &[u8]) -> Result<ProcessedImage> {
    let image = Reader::new(Cursor::new(buf))
        .with_guessed_format()
        .map_err(Error::InvalidImageFormat)?
        .decode()
        .map_err(Error::ImageDecodingError)?;
    let mut reencoded = Vec::new();
    image.write_to(&mut reencoded, ImageOutputFormat::Png)?;
    let width = image.width() as u32;
    let height = image.height() as u32;
    let dimensions = (width, height);
    if width < 100 || height < 100 {
        return Ok(ProcessedImage {
            reencoded,
            thumbnail: None,
            dimensions,
        });
    }
    let to_thumb = if width == height {
        image
    } else {
        let min = width.min(height);
        let max = width.max(height);
        let offset_by = (max - min) / 2;
        if width > height {
            image.crop_imm(offset_by, 0, min, min)
        } else {
            image.crop_imm(0, offset_by, min, min)
        }
    };
    let thumb = to_thumb.thumbnail_exact(100, 100);
    let mut thumbnail = Vec::new();
    thumb.write_to(&mut thumbnail, ImageOutputFormat::Jpeg(20))?;
    Ok(ProcessedImage {
        reencoded,
        thumbnail: Some(thumbnail),
        dimensions,
    })
}
